package DLX

import (
	"fmt"
	"os"
	"strconv"
)

const MemSize = 10000 // bytes in memory (divisible by 4)

// Mnemonic-to-Opcode mapping
var mnemo = [...]string{
	"ADD", "SUB", "MUL", "DIV", "MOD", "CMP", "ERR", "ERR", "OR", "AND", "BIC", "XOR", "LSH", "ASH", "CHK", "ERR",
	"ADDI", "SUBI", "MULI", "DIVI", "MODI", "CMPI", "ERRI", "ERRI", "ORI", "ANDI", "BICI", "XORI", "LSHI", "ASHI", "CHKI", "ERR",
	"LDW", "LDX", "POP", "ERR", "STW", "STX", "PSH", "ERR", "BEQ", "BNE", "BLT", "BGE", "BLE", "BGT", "BSR", "ERR",
	"JSR", "RET", "RDI", "WRD", "WRH", "WRL", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR",
	"ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR", "ERR"}

const ADD = 0
const SUB = 1
const MUL = 2
const DIV = 3
const MOD = 4
const CMP = 5
const OR = 8
const AND = 9
const BIC = 10
const XOR = 11
const LSH = 12
const ASH = 13
const CHK = 14

const ADDI = 16
const SUBI = 17
const MULI = 18
const DIVI = 19
const MODI = 20
const CMPI = 21
const ORI = 24
const ANDI = 25
const BICI = 26
const XORI = 27
const LSHI = 28
const ASHI = 29
const CHKI = 30

const LDW = 32
const LDX = 33
const POP = 34
const STW = 36
const STX = 37
const PSH = 38

const BEQ = 40
const BNE = 41
const BLT = 42
const BGE = 43
const BLE = 44
const BGT = 45
const BSR = 46
const JSR = 48
const RET = 49

const RDI = 50
const WRD = 51
const WRH = 52
const WRL = 53

const ERR = 63 // error opcode which is insertered by loader
// after end of program code

type Dlx struct {
	R      [32]int
	PC     int
	Op     int
	A      int
	B      int
	C      int
	Format int

	// emulated memory
	M [MemSize / 4]int
}

func (d *Dlx) load(program []int) {
	for i, _ := range program {
		d.M[i] = program[i]
	}

	d.M[len(program)] = -1 // set first opcode of first instruction after program
	// to ERR in order to detect 'fall off the edge' errors
}

func (d *Dlx) execute() {
	var origc int = 0 // used for F2 instruction RET
	for i := 0; i < 32; i++ {
		d.R[i] = 0
	}
	d.PC = 0
	d.R[30] = MemSize - 1

execloop:
	for {
		d.R[0] = 0
		d.disassem(d.M[d.PC]) // initializes op, a, b, c

		var nextPC int = d.PC + 1
		if d.Format == 2 {
			origc = d.C    // used for RET
			d.C = d.R[d.C] // dirty trick
		}
		switch d.Op {
		case ADDI, ADD:
			d.R[d.A] = d.R[d.B] + d.C

		case SUBI, SUB:
			d.R[d.A] = d.R[d.B] - d.C

		case CMPI, CMP:
			d.R[d.A] = d.R[d.B] - d.C // can not create overflow
			if d.R[d.A] < 0 {
				d.R[d.A] = -1
			} else if d.R[d.A] > 0 {
				d.R[d.A] = 1
			}
			// we don't have to do anything if R[a]==0

		case MUL, MULI:
			d.R[d.A] = d.R[d.B] * d.C

		case DIV, DIVI:
			d.R[d.A] = d.R[d.B] / d.C

		case MOD, MODI:
			d.R[d.A] = d.R[d.B] % d.C

		case OR, ORI:
			d.R[d.A] = d.R[d.B] | d.C

		case AND, ANDI:
			d.R[d.A] = d.R[d.B] & d.C

		case BIC, BICI:
			d.R[d.A] = d.R[d.B] & ^d.C

		case XOR, XORI:
			d.R[d.A] = d.R[d.B] ^ d.C
		// Shifts: - a shift by a positive number means a left shift
		//         - if c > 31 or c < -31 an error is generated

		case LSH, LSHI:
			if (d.C < -31) || (d.C > 31) {
				fmt.Println("Illegal value ", d.C, " of operand c or register c!")
				bug(1)
			}
			if d.C < 0 {
				d.R[d.A] = d.R[d.B] >> -d.C
			} else {
				d.R[d.A] = d.R[d.B] << d.C
			}

		case ASH, ASHI:
			if (d.C < -31) || (d.C > 31) {
				fmt.Println("DLX.execute: Illegal value ", d.C, " of operand c or register c!")
				bug(1)
			}
			if d.C < 0 {
				d.R[d.A] = d.R[d.B] >> -d.C
			} else {
				d.R[d.A] = d.R[d.B] << d.C
			}

		case CHKI, CHK:
			if d.R[d.A] < 0 {
				fmt.Println("DLX.execute: ", d.PC*4, ": R[", d.A, "] == ", d.R[d.A], " < 0")
				bug(14)
			} else if d.R[d.A] >= d.C {
				fmt.Println("DLX.execute: ", d.PC*4, ": R[", d.A, "] == ", d.R[d.A], " >= ", d.C)
				bug(14)
			}

		case LDW, LDX: // remember: c == R[origc] because of F2 format
			d.R[d.A] = d.M[(d.R[d.B]+d.C)/4]

		case STW, STX: // remember: c == R[origc] because of F2 format
			d.M[(d.R[d.B]+d.C)/4] = d.R[d.A]

		case POP:
			d.R[d.A] = d.M[d.R[d.B]/4]
			d.R[d.B] = d.R[d.B] + d.C

		case PSH:
			d.R[d.B] = d.R[d.B] + d.C
			d.M[d.R[d.B]/4] = d.R[d.A]

		case BEQ:
			if d.R[d.A] == 0 {
				nextPC = d.PC + d.C
			}
			if (nextPC < 0) || (nextPC > MemSize/4) {
				fmt.Println(4*nextPC, " is no address in memory (0..", MemSize, ").")
				bug(40)
			}
		case BNE:
			if d.R[d.A] != 0 {
				nextPC = d.PC + d.C
			}
			if (nextPC < 0) || (nextPC > MemSize/4) {
				fmt.Println(4*nextPC, " is no address in memory (0..", MemSize, ").")
				bug(41)
			}

		case BLT:
			if d.R[d.A] < 0 {
				nextPC = d.PC + d.C
			}
			if (nextPC < 0) || (nextPC > MemSize/4) {
				fmt.Println(4*nextPC, " is no address in memory (0..", MemSize, ").")
				bug(42)
			}
		case BGE:
			if d.R[d.A] >= 0 {
				nextPC = d.PC + d.C
			}
			if (nextPC < 0) || (nextPC > MemSize/4) {
				fmt.Println(4*nextPC, " is no address in memory (0..", MemSize, ").")
				bug(43)
			}

		case BLE:
			if d.R[d.A] <= 0 {
				nextPC = d.PC + d.C
			}
			if (nextPC < 0) || (nextPC > MemSize/4) {
				fmt.Println(4*nextPC, " is no address in memory (0..", MemSize, ").")
				bug(44)
			}

		case BGT:
			if d.R[d.A] > 0 {
				nextPC = d.PC + d.C
			}
			if (nextPC < 0) || (nextPC > MemSize/4) {
				fmt.Println(4*nextPC, " is no address in memory (0..", MemSize, ").")
				bug(45)
			}

		case BSR:
			d.R[31] = (d.PC + 1) * 4
			nextPC = d.PC + d.C

		case JSR:
			d.R[31] = (d.PC + 1) * 4
			nextPC = d.C / 4

		case RET:
			if origc == 0 {
				goto execloop // remember: c==R[origc]
			}
			if (d.C < 0) || (d.C > MemSize) {
				fmt.Println(d.C, " is no address in memory (0..", MemSize, ").")
				bug(49)
			}
			nextPC = d.C / 4

		case RDI:
			fmt.Print("?: ")
			var i int
			fmt.Scan(&i)
			d.R[d.A] = i

		case WRD:
			fmt.Print(d.R[d.B], "  ")

		case WRH:
			h := fmt.Sprintf("%x", d.R[d.B])
			fmt.Print("0x", h, " ")

		case WRL:
			fmt.Println()

		case ERR:
			fmt.Println("Program dropped off the end!")
			bug(1)

		default:
			fmt.Println("DLX.execute: Unknown opcode encountered!")
			bug(1)
		}
		d.PC = nextPC
	}
}

func (d *Dlx) disassem(instructionWord int) {
	d.Op = instructionWord >> 26 // without sign extension
	switch d.Op {

	// F1 Format
	case BSR, RDI, WRD, WRH, WRL, CHKI, BEQ, BNE, BLT, BGE, BLE, BGT, ADDI, SUBI, MULI, DIVI, MODI, CMPI, ORI, ANDI, BICI, XORI, LSHI, ASHI, LDW, POP, STW, PSH:
		d.Format = 1
		d.A = (instructionWord >> 21) & 0x1F
		d.B = (instructionWord >> 16) & 0x1F
		// TODO convert to short!
		d.C = instructionWord // another dirty trick

	// F2 Format
	case RET, CHK, ADD, SUB, MUL, DIV, MOD, CMP, OR, AND, BIC, XOR, LSH, ASH, LDX, STX:
		d.Format = 2
		d.A = (instructionWord >> 21) & 0x1F
		d.B = (instructionWord >> 16) & 0x1F
		d.C = instructionWord & 0x1F

	// F3 Format
	case JSR:
		d.Format = 3
		d.A = -1 // invalid, for error detection
		d.B = -1
		d.C = instructionWord & 0x3FFFFFF

	// unknown instruction code
	default:
		fmt.Println("Illegal instruction! (", d.PC, ")")
	}
}

func (d *Dlx) disassemble(instructionWord int) string {

	d.disassem(instructionWord)
	var line string = mnemo[d.Op] + "  "

	switch d.Op {
	case WRL:
		line += "\n"
		return line

	case JSR, BSR, RET:
		line += strconv.Itoa(d.C)
		line += "\n"
		return line
	case RDI:
		line += strconv.Itoa(d.A) + "\n"
		return line

	case WRD, WRH:
		line += strconv.Itoa(d.B) + "\n"
		return line

	case BEQ, BNE, BLT, BGE, BLE, BGT, CHKI, CHK:
		line += strconv.Itoa(d.A) + " " + d.C + "\n"
		return line
	case ADDI, SUBI, MULI, DIVI, MODI, CMPI, ORI, ANDI, BICI, XORI, LSHI, ASHI, LDW, POP, STW, PSH, ADD, SUB, MUL, DIV, MOD, CMP, OR, AND, BIC, XOR, LSH, ASH, LDX, STX:
		line += strconv.Itoa(d.A) + " " + d.B + " " + d.C + "\n"
		return line
	default:
		line += "\n"
		return line
	}
}

func assemble(op int) int {
	if op != WRL {
		fmt.Println("DLX.assemble: the only instruction without arguments is WRL!")
		bug(1)
	}
	return F1(op, 0, 0, 0)
}

func assemble2(op, arg1 int) int {
	switch op {
	// F1 Format
	case BSR:
		return F1(op, 0, 0, arg1)
	case RDI:
		return F1(op, arg1, 0, 0)
	case WRH, WRD:
		return F1(op, 0, arg1, 0)

	// F2 Format
	case RET:
		return F2(op, 0, 0, arg1)

	// F3 Format
	case JSR:
		return F3(op, arg1)
	default:
		fmt.Println("DLX.assemble: wrong opcode for one arg instruction!")
		bug(1)
		return -1 // java forces this senseless return statement!
		// I'm thankful for every sensible explanation.
	}
}

func assemble3(op, arg1, arg2 int) int {
	switch op {

	// F1 Format
	case CHKI, BEQ, BNE, BLT, BGE, BLE, BGT:
		return F1(op, arg1, 0, arg2)

	// F2 Format
	case CHK:
		return F2(op, arg1, 0, arg2)

	default:
		fmt.Println("DLX.assemble: wrong opcode for two arg instruction!")
		bug(1)
		return -1
	}
}

func assemble4(op, arg1, arg2, arg3 int) int {
	switch op {
	// F1 Format
	case ADDI, SUBI, MULI, DIVI, MODI, CMPI, ORI, ANDI, BICI, XORI, LSHI, ASHI, LDW, POP, STW, PSH:
		return F1(op, arg1, arg2, arg3)

	// F2 Format
	case ADD, SUB, MUL, DIV, MOD, CMP, OR, AND, BIC, XOR, LSH, ASH, LDX, STX:
		return F2(op, arg1, arg2, arg3)

	default:
		fmt.Println("DLX.assemble: wrong opcode for three arg instruction!")
		bug(1)
		return -1
	}
}

func F1(op, a, b, c int) int {
	if c < 0 {
		c ^= 0xFFFF0000
	}
	if (a & ^0x1F | b & ^0x1F | c & ^0xFFFF) != 0 {
		fmt.Println("Illegal Operand(s) for F1 Format.")
		bug(1)
	}
	return op<<26 | a<<21 | b<<16 | c
}

func F2(op, a, b, c int) int {
	if (a & ^0x1F | b & ^0x1F | c & ^0x1F) != 0 {
		fmt.Println("Illegal Operand(s) for F2 Format.")
		bug(1)
	}
	return op<<26 | a<<21 | b<<16 | c
}

func F3(op, c int) int {
	if (c < 0) || (c > MemSize) {
		fmt.Println("Operand for F3 Format is referencing " + "non-existent memory location.")
		bug(1)
	}
	return op<<26 | c
}

func bug(n int) {
	fmt.Println("bug number: ", n)
	os.Exit(n)
}
